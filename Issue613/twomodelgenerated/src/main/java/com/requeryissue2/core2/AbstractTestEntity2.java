package com.requeryissue2.core2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by dramo on 26/07/2017.
 */


@Entity()
@Table(name = "TestEntity")
public abstract class AbstractTestEntity2 {

    @Id
    @Column(nullable = false)
    protected Long id;

}
