package com.requeryissue;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Set;

import io.requery.android.sqlite.DatabaseSource;
import io.requery.cache.EmptyEntityCache;
import io.requery.meta.EntityModel;
import io.requery.query.Result;
import io.requery.sql.Configuration;
import io.requery.sql.ConfigurationBuilder;
import io.requery.sql.EntityDataStore;
import io.requery.sql.SchemaModifier;
import io.requery.sql.TableCreationMode;
import io.requery.sql.platform.SQLite;

@RunWith(AndroidJUnit4.class)

public class IssueTest {

    private static final String DATABASE_NAME = "db";
    private EntityDataStore entityDataStore;

    @Before
    public void setup() {

        Context appContext = InstrumentationRegistry.getTargetContext();

        // Create database source
        EntityModel entityModel = Models.REQUERYISSUE;

        DatabaseSource dataSource = new DatabaseSource(appContext, entityModel, DATABASE_NAME, 1);
        dataSource.setLoggingEnabled(true);

        SQLite platform = new SQLite();

        // Create configuration:
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder(dataSource, entityModel)
        .useDefaultLogging()
        .setEntityCache(new EmptyEntityCache()) // no cache
        .setPlatform(platform);

        // Create entity data store
        Configuration configuration = configurationBuilder.build();
        entityDataStore = new EntityDataStore(configuration);

        SchemaModifier tables = new SchemaModifier(configuration);
        tables.dropTables();
        TableCreationMode mode = TableCreationMode.CREATE;
        tables.createTables(mode);

    }

    @After
    public void teardown() {
        if (entityDataStore != null) {
            entityDataStore.close();
        }
    }

    /**
     * This test create and insert a TestEntity with a @OneToMany relation to RelatedTestEntity.
     * @OneToMany cascade is defined as @OneToMany(cascade = {CascadeType.ALL})
     * It checks that when TestEntity is inserted, the RelatedTestEntity insert is not propagate as expected.
     * This happens because the JPA CascadeType.ALL generates requery .setCascadeAction(CascadeAction.NONE,CascadeAction.SAVE,CascadeAction.DELETE)
     * Because of CascadeAction.NONE the RelatedTestEntity is not saved on TestEntity save.
     * Extracted generated RelatedTestEntity:
    public static final Attribute<TestEntity, Set<RelatedTestEntity>> RELATED_ENTITIES =
    new SetAttributeBuilder<TestEntity, Set<RelatedTestEntity>, RelatedTestEntity>("relatedEntities", Set.class, RelatedTestEntity.class)
    .setProperty(new Property<TestEntity, Set<RelatedTestEntity>>() {
    @Override
    public Set<RelatedTestEntity> get(TestEntity entity) {
    return entity.relatedEntities;
    }

    @Override
    public void set(TestEntity entity, Set<RelatedTestEntity> value) {
    entity.relatedEntities = value;
    }
    })
    .setPropertyName("relatedEntities")
    .setPropertyState(new Property<TestEntity, PropertyState>() {
    @Override
    public PropertyState get(TestEntity entity) {
    return entity.$relatedEntities_state;
    }

    @Override
    public void set(TestEntity entity, PropertyState value) {
    entity.$relatedEntities_state = value;
    }
    })
    .setGenerated(false)
    .setReadOnly(true)
    .setLazy(false)
    .setNullable(true)
    .setUnique(false)
    .setCascadeAction(CascadeAction.NONE,CascadeAction.SAVE,CascadeAction.DELETE)
    .setCardinality(Cardinality.ONE_TO_MANY)
    .setMappedAttribute(new Supplier<Attribute>() {
    @Override
    public Attribute get() {
    return RelatedTestEntity.PARENT_ENTITY;
    }
    })
    .build();

     */
    @Test
    public void testInsertCascadeAll() {
        TestEntity entity = new TestEntity();
        entity.setId(new Long(10));
        RelatedTestEntity relatedEntity = new RelatedTestEntity();
        relatedEntity.setId(new Long(100));
        entity.getRelatedEntities().add(relatedEntity);

        entityDataStore.insert(entity);

        List<TestEntity> loadedEntities = ((Result<TestEntity>) entityDataStore.select(TestEntity.class).get()).toList();

        Assert.assertNotNull(loadedEntities);
        Assert.assertEquals(loadedEntities.size(),1);

        TestEntity loadedEntity = loadedEntities.get(0);
        Set<RelatedTestEntity> relatedEntities = loadedEntity.getRelatedEntities();

        Assert.assertNotNull(relatedEntities);
        Assert.assertEquals(relatedEntities.size(),1);
    }
}
