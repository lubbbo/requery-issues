package com.requeryissue;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Created by dramo on 26/07/2017.
 */

@Entity()
@Table(name = "TestEntity")
public abstract class AbstractTestEntity {

    @Id
    @Column(nullable = false)
    protected Long id;

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE, CascadeType.DETACH})
    protected Set<RelatedTestEntity> relatedEntities;
}
