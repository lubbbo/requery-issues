package com.requeryissue;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by dramo on 26/07/2017.
 */
@Entity()
@Table(name = "RelatedTestEntity")
public abstract class AbstractRelatedTestEntity {

    @Id
    @Column(nullable = false)
    protected Long id;

    @Column(nullable = false)
    @ManyToOne
    protected TestEntity parentEntity;
}
