package com.requeryissue;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Set;

import io.requery.android.sqlite.DatabaseSource;
import io.requery.cache.EmptyEntityCache;
import io.requery.meta.EntityModel;
import io.requery.query.Result;
import io.requery.sql.Configuration;
import io.requery.sql.ConfigurationBuilder;
import io.requery.sql.EntityDataStore;
import io.requery.sql.SchemaModifier;
import io.requery.sql.TableCreationMode;
import io.requery.sql.platform.SQLite;

@RunWith(AndroidJUnit4.class)

public class IssueTest {

    private static final String DATABASE_NAME = "db";
    private EntityDataStore entityDataStore;

    @Before
    public void setup() {

        Context appContext = InstrumentationRegistry.getTargetContext();

        // Create database source
        EntityModel entityModel = Models.REQUERYISSUE;

        DatabaseSource dataSource = new DatabaseSource(appContext, entityModel, DATABASE_NAME, 1);
        dataSource.setLoggingEnabled(true);

        SQLite platform = new SQLite();

        // Create configuration:
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder(dataSource, entityModel)
        .useDefaultLogging()
        .setEntityCache(new EmptyEntityCache()) // no cache
        .setPlatform(platform);

        // Create entity data store
        Configuration configuration = configurationBuilder.build();
        entityDataStore = new EntityDataStore(configuration);

        SchemaModifier tables = new SchemaModifier(configuration);
        tables.dropTables();
        TableCreationMode mode = TableCreationMode.CREATE;
        tables.createTables(mode);

    }

    @After
    public void teardown() {
        if (entityDataStore != null) {
            entityDataStore.close();
        }
    }

    /**
     * This test create and insert a TestEntity with a @OneToMany relation to RelatedTestEntity.
     * It checks that when TestEntity is deleted, the operation fails probably because,
     * Requery tries to delete the RelatedTestEntity that has been already deleted by ON DELETE CASCADE contratint by SQLite.
     * SQL DB deletion doesn't fail but returns 0 affected rows and let Requery operation ending with failure after its checkRowsAffected.
     * Stack trace:
     io.requery.sql.RowCountException: Expected 1 row affected actual 0
     at io.requery.sql.EntityWriter.checkRowsAffected(EntityWriter.java:182)
     at io.requery.sql.EntityWriter.delete(EntityWriter.java:874)
     at io.requery.sql.EntityWriter.cascadeRemove(EntityWriter.java:960)
     at io.requery.sql.EntityWriter.clearAssociations(EntityWriter.java:910)
     at io.requery.sql.EntityWriter.delete(EntityWriter.java:872)
     at io.requery.sql.EntityDataStore.delete(EntityDataStore.java:343)
     at com.requeryissue.IssueTest.testDeletion(IssueTest.java:103)
     at java.lang.reflect.Method.invoke(Native Method)
     at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:50)
     at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)
     at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:47)
     at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)
     at org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)
     at org.junit.internal.runners.statements.RunAfters.evaluate(RunAfters.java:27)
     at org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:325)
     at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:78)
     at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:57)
     at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)
     at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)
     at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)
     at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)
     at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)
     at org.junit.runners.ParentRunner.run(ParentRunner.java:363)
     at org.junit.runners.Suite.runChild(Suite.java:128)
     at org.junit.runners.Suite.runChild(Suite.java:27)
     at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)
     at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)
     at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)
     at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)
     at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)
     at org.junit.runners.ParentRunner.run(ParentRunner.java:363)
     at org.junit.runner.JUnitCore.run(JUnitCore.java:137)
     at org.junit.runner.JUnitCore.run(JUnitCore.java:115)
     at android.support.test.internal.runner.TestExecutor.execute(TestExecutor.java:59)
     at android.support.test.runner.AndroidJUnitRunner.onStart(AndroidJUnitRunner.java:262)
     at android.app.Instrumentation$InstrumentationThread.run(Instrumentation.java:1932)

     */
    @Test
    public void testDeletionOneToManyCascade() {
        TestEntity entity = new TestEntity();
        entity.setId(new Long(10));
        RelatedTestEntity relatedEntity = new RelatedTestEntity();
        relatedEntity.setId(new Long(100));
        entity.getRelatedEntities().add(relatedEntity);

        entityDataStore.insert(entity);

        List<TestEntity> loadedEntities = ((Result<TestEntity>) entityDataStore.select(TestEntity.class).get()).toList();

        Assert.assertNotNull(loadedEntities);
        Assert.assertEquals(loadedEntities.size(),1);

        TestEntity loadedEntity = loadedEntities.get(0);
        Set<RelatedTestEntity> relatedEntities = loadedEntity.getRelatedEntities();

        Assert.assertNotNull(relatedEntities);
        Assert.assertEquals(relatedEntities.size(),1);

        RelatedTestEntity loadedRelatedEntity = relatedEntities.iterator().next();

        Assert.assertNotNull(loadedRelatedEntity);

        entityDataStore.delete(loadedEntity);

        List<TestEntity> reloadedEntities = ((Result<TestEntity>) entityDataStore.select(TestEntity.class).get()).toList();
        Assert.assertNotNull(reloadedEntities);
        Assert.assertEquals(reloadedEntities.size(),0);
    }
}
